private String department;

public Manager(String name, double salary, String department) {
        super(name, salary);
        this.department = department;
        }

public void setDepartment(String department) {
        this.department = department;
        }

public String getDepartment() {
        return department;
        }

@Override
public void getDetails() {
        System.out.println("Name: " + getName());
        System.out.println("Salary: $" + getSalary());
        System.out.println("Department: " + department);
        }